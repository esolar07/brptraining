  <?php $page = "Sign Up"; ?>

  <?php include_once "brpheader.php" ?>
 
  <div class="row">
 
    <!-- Contact Details -->
    <div class="large-9 columns">
 
      <h3 style="color:red;" class="contactmain">More Info</h3>
	    <section class="section">
        <h5 class="title"><a href="#panel2" style="color:yellow;">Bryon Rizzi</a></h5>
        <div class="content" data-slug="panel2">
          <ul class="large-block-grid-5">
            <li><a href="mailto:BRPtraining@yahoo.com" style="color:yellow;"><img src="bryon.jpeg">Email</a></li>
          </ul>
        </div>
		</section>

            <!-- Contact Form -->
      <div class="section-container tabs contactmain" data-section>
        <section class="section">
          <h5 class="title"><a href="#panel1"style="color:red;">Contact BRPtraining</a></h5>
          <div id='form' class="content" data-slug="panel1">
            <form action="confirm.php" method="post">
              <div class="row collapse">
                <div class="large-2 columns">
                  <label class="inline" style="color:yellow;">Your Name</label>
                </div>
                <div class="large-10 columns">
                  <input type="text" id="yourName" placeholder="Name" name="cus_name" class="required">
                  <span>Please enter your name</span> 
                </div>
              </div><br>
              <div class="row collapse">
                <div class="large-2 columns">
                  <label class="inline" style="color:yellow;"> Your Email</label>
                </div>
                <div class="large-10 columns">
                  <input type="text" id="yourEmail" placeholder="email" name="email" class="required">
                  <span>Enter a valid email </span>
                </div>
              </div><br>
              <label style="color:yellow;">Message</label>
              <textarea rows="10" cols="30" name="comments" class="required"></textarea>
              <span>Enter your message</span><br><br>
              <div class="submit">
                <button type="submit" value="Submit" class="radius button">Submit</button>
              </div>
            </form>
          </div>
        </section>
      </div>
    </div>
  
  <script type="text/javascript" src="JS/jquery.js"></script>
    <script type="text/javascript" src="JS/validEmail.js"></script>
    <script type="text/javascript">

      var $submit = $('.submit');
      var $required = $('.required');
      
      function containsBlank(){
        var blanks = new Array();
        $required.each(function(){
          blanks.push($(this).val() == "");
        });
        // for older versions of ie return blanks.sort().pop();
        return $.inArray(true, blanks) !== -1;
      }

      function requiredFillIn(){
        if (containsBlank() || !$('#yourEmail').validEmail()) 
          $submit.attr("disabled", "disabled");
        else 
          $submit.removeAttr("disabled");
      }

      $('#form span').hide();
      $('input, textarea').focus(function(){
        $(this).next().fadeIn("slow");
      }).blur(function(){
        $(this).next().fadeOut("slow");
      }).keyup(function(){
        //check required fields
        requiredFillIn();
      });

      $("#yourEmail").validEmail({on:'keyup', success:function(){
        $(this).next().removeClass("error").addClass('valid');
      }, failure: function(){
        $(this).next().removeClass("valid").addClass('error');
      }});

      requiredFillIn();
    </script>
  <?php  include_once "brpfooter.php" ?>
