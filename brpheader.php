<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="keywords" content="Bryon Rizzi, Training, Working Out, Miami Beach Training, Equinox Miami Beach">
    <title><?php echo "BRPtraining - ".$page; ?></title>
    <link rel="stylesheet" href="foundation.css" />
    <link rel="stylesheet" href="webicons.css" />
	  <link rel="stylesheet" href='brp.css' />
    <link rel="stylesheet" href='animate-custom.css' />
	  <link href='http://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>
    <script href="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
  <!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
  <script type="text/javascript" src="jquery-1.10.2.min.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"></script>
  <script src="modernizr.js"></script>

  </head>
  <body>

  <?php if ($page != "BELIEVE II ACHIEVE"){
     include_once"nav.php"; 
  } 
  ?>
