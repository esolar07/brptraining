       <!-- Navigation -->
    <nav class="top-bar" data-topbar>
      <ul class="title-area">
        <!-- Title Area -->
        <li class="name">
				  <img src='logo.jpg' height="70" width="80"/>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
      </ul>
 
      <section class="top-bar-section">
        <ul class="left">
          <li><a href="home.php">HOME</a></li>
          <li><a href="about.php">VISION</a></li>
          <li><a href="meet.php">MEET BRP</a></li>
          <li><a href="services.php">SERVICES</a></li>
          <!-- romved until testimonals are provided <li><a href="#">TRAINING TESTIMONIALS</a></li> -->
          <li><a href="contact.php">BOOK NOW</a></li>
        </ul>
      </section>
    </nav>
    <!-- End Navigation -->