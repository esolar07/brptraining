  <?php $page = "Services"; ?>

  <?php  include_once "brpheader.php"; ?>

    <!-- Main Content Section -->
    <div id ="sertop" class="row">
      <div class="large-12 columns">
        <p class="animated flipInX">- 1 on 1 Consulting</p>
        <p class="animated flipInX">- Small group/team training</p>
        <p class="animated flipInX">- Sports Specific</p>
        <p class="animated flipInX">- Program Design</p>
        <p class="animated flipInX">- Strength and Conditioning</p>
      </div>
    </div>

    <div class="row srow" >
      <div class="large-4 columns">
         <img src="s-image1.jpeg" />
      </div>    
      <div class="large-8 columns">
        <h4>PERSONAL CONSULTING</h4>
        <p>Showing our clients the way to live a healthy lifestye is what we pride our self on each day. Taking advise from BRPtraining is not only training or coaching tips, this is a relationship with tips that can be helpful through out the daily grind of life.</p>
      </div>
    </div>
	
	   <br><br>
	
    <div class="row srow">
      
      <div class="large-4 columns">
         <img src="s-image2.jpeg" />
      </div>
      
      <div class="large-8 columns">
        <h4>PROGRAM DESIGN</h4>
        <p>Each client or athlete will get a program specalized for each ones needs under our supervision, also will be offered online programs  or take home programs only if  1 on 1 training isn't able to take place. This is also a tool that can be used for clients or athletes/teams that may need a program when traveling.</p>
      </div>
    </div>
	
  	<br><br>
	
    <div class="row srow">
      <div class="large-4 columns">
        <img src="s-image3.jpeg" />
      </div>
      <div class="large-8 columns">
        <h4>GROUP TRAINING</h4>
        <p>This is not "bootcamp". Groups larger then 4 is what we call our "fit program" We also do 2 on 1 or 3-4 on 1 training as well as team training for programs needing extra coaching in Srength and Conditioning for summer or fall.</p>
      </div>
    </div>
	
	  <br><br>
	
    <div class="row srow">
      <div class="large-4 columns">
        <img src="s-image4.jpeg" />
      </div>
      <div class="large-8 columns">
        <h4>SPORTS SPECIFIC</h4>
        <p>This program is 5-6  days but can be mixed and matched for teams and athletes availability. This can be catered for middle school-professional athletes/teams of all levels. Each individual day will be broken into 4 quarters with overtime (mental Prep) if needed. Depending on that days focus  within each workout, we focus on Speed, Agility (COD) and Power.</p>
      </div>
    </div>
	
	  <br><br>
	
    <div class="row srow">  
      <div class="large-4 columns">
        <img src="s-image5.jpeg" />
      </div>  
      <div class="large-8 columns">
        <h4>STRENGTH AND CONDITIONING</h4>
        <p>Strength – the extent to which muscles can exert force by contracting against resistance (holding or restraining an object or person). Conditioning- the bodies ability to work easily and/or with lessened stress while performing a chosen activity or exercise. At BRP we combine the two and maximize each client or athletes goals by strength and condinditioning (systems of the body) with injury proventive exercises to reach the goal set. This is for athletes as well as are non athletes.</p>
      </div>
    </div>
	
	  <br><br>

  <?php include_once "brpfooter.php"; ?>

