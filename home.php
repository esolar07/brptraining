  <?php $page = "Home"; ?>
  <?php include_once "brpheader.php"; ?>
    
    <!-- Slider Out Till Further Notice -->

    <div class="row homeslide">
      <div class="large-12 columns">
        <div data-orbit id="slider">
          <img src="image1.jpeg" />
          <img src="image3.jpg" />
          <img src="image2.jpg" />
          <img src="image4.jpg" />
        </div>
        <hr />
      </div>
    </div>
  
    <div class="row">
      <div class="large-12 columns">
      	<!--<div class="panel">-->
	        <h3 style='color:red;'>Welcome to BRP</h3>
	        <p style='color:yellow;'>Welcome to BRPtraining. My name is Bryon Rizzi, founder and consultant. Fitness or "muscle movement" and Sports Specific coaching is a passion I have taken head on since I started this journey at the age of 13. Playing high school sports and being fortunate to earn a scholarship to the University of Findlay to play college football as well as study Strength and Conditioning is where the real world in fitness training and coaching started to become clear.<a href="contact.php"><b style="color:red;">BELIEVE II ACHIEVE</b></a></p>
	        <p style='color:yellow;'>Book a first time session</p><br>
			    <a href="contact.php" class="small round button ">BOOK NOW</a><br>
      	

    <div class="row">
      <div class="large-6 medium-6 columns">
        <h4 style="color:red">ABOUT BRP</h4>
       <img src="s-image4.jpeg" height="350" width="350" />
      </div>
      <div id="list" class="large-6 medium-6 columns">
        <p style='color:yellow;' class="animated flipInX">- Weight Loss</p>
        <p style='color:yellow;' class="animated flipInX">- Nutritional Advice</p>
        <p style='color:yellow;' class="animated flipInX">- Muscle Toning</p>
        <p style='color:yellow;' class="animated flipInX">- Strength and Conditioning</p>
        <p style='color:yellow;' class="animated flipInX">- 1 on 1 Consulting</p>
        <p style='color:yellow;' class="animated flipInX">- Program Design</p>
        <p style='color:yellow;' class="animated flipInX">- Sports Specific Training</p>
        <p style='color:yellow;' class="animated flipInX">- NFL Combine Training</p>
      </div>
</div>
<div class="row">
  <div class="large-12 columns">
     <hr>
     <p style='color:yellow;'>BRP training is a fitness and consulting company driven by results! Each client under our supervision will get state-­‐of-­‐the-­‐art professional training and consulting aimed to exceed achieving your fitness goals. Backed by science with injury preventative methods, we’re proven to take the body mentally and physically to the next level. For the last 6 plus years, BRP training has helped clients do everything including, losing weight, gaining muscle, increase speed, increase vertical jump, significantly increased strength gain, dropped body fat and assistance with individual lifestyle habits to instill a positive way of living and staying fit.  </p>
  </div>
</div>


</div>
      </div>
    </div>
    
    <div class="row">
      <div class="large-5 medium-5 columns">
        <h4>Videos</h4>
        <div class="flex-video">
          <iframe width="560" height="315" src="//www.youtube.com/embed/G6-Mbr5W2aQ?rel=0" frameborder="0" allowfullscreen></iframe>
        </div>
      </div>
      <div class="large-7 medium-7 columns">
        <h4>Contact BRP<h4>
        <p>Miami, FL 33132<br><a href="mailto:BRPtraining@yahoo.com" style="color:yellow;">BRPtraining@yahoo.com</a><br>Tel: 904-377-0918</p>
        <a href="http://www.facebook.com/b_rizzi" ><img src="webicon-facebook.png"></a>
        <a href="https://twitter.com/B_rizzi21" ><img src="webicon-twitter.png"></a>
        <a href="http://instagram.com/b_rizzi" ><img src="webicon-instagram.png"></a>
      </div>        
    </div>
  <?php include_once "brpfooter.php"; ?>
