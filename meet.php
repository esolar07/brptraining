  <?php $page = "Home"; ?>
  <?php include_once "brpheader.php"; ?>

<div class="row homeslide">
      <div class="large-6 medium-6 columns">
        <h4>MEET BRP</h4>
       <img src="s-image4.jpeg" height="350" width="350" />
      </div>
      <div class="large-6 medium-6 columns">
        <p class="animated flipInX">- B.S Strength and Conditioning</p>
        <p class="animated flipInX">- Athletic Scholarship</p>
        <p class="animated flipInX">- Certified in Strength and Conditioning for Performance(CSCS)</p>
        <p class="animated flipInX">- Certified in Personal Training (ASFA)</p>
        <p class="animated flipInX">- Tier 3 Trainer</p>
        <p class="animated flipInX">- KB Level 1 / TRX Level 1 Cert.</p>
        <p class="animated flipInX">- Sports Specific Training</p>
        <p class="animated flipInX">- NFL Combine Training</p>
      </div>
</div>
<div class="row">
  <div class="large-12 columns">
     <hr>
     <p>Bryon has trained since the age of 13 with the mindset that you have to <a href="contact.php"><b style="color:red;">BELIEVE II ACHIEVE</b></a>. After achieving all confernce and state honors, state championship in football and second place in the 169 weight class for power lifting in high school. From there, Bryon earned an athletic scholarship to play football for the University of Findlay. While at Findlay, he finished his career with 33 straight starts, 200 tackles and five interceptions. With the desire to learn more about the science of exercising drove him to study Strength and Conditioning. After graduating, Bryon’s focus turned to help clients achieve their fitness goals. He’s now certified in Personal Training (ASFA) and Strength and Conditioning which allows him to help clients get to any fitness goal they may have. While helping clients from around the globe for the last decade, Bryon can say, “I’ll make you believe and help you achieve!” </p>
  </div>
</div>

  <?php include_once "brpfooter.php"; ?>